package com.sourceit.apitask1;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

import android.os.Bundle;

public class MainActivity extends AppCompatActivity implements ActivityNavigation{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.main_container, InputDateFragment.newInstance())
                .commit();
    }

    @Override
    public void showSingleCurrencyFragment(RequestModel.ExchangeRate currency) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.main_container, new CurrencyFragment(currency))
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void showCurrencyFragment(String inputDate) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.main_container, new ListFragment(inputDate))
                .addToBackStack(null)
                .commit();
    }
}
