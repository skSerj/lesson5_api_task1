package com.sourceit.apitask1;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ListFragment extends Fragment implements OnCurrencyClickListener {

    private RecyclerView recyclerView;
    private CurrencyAdapter currencyAdapter;
    private String date;
    private ActivityNavigation navigation;

    private List<RequestModel.ExchangeRate> rateList;

    public ListFragment(String dateOfRates) {
        date = dateOfRates;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof ActivityNavigation) {
            navigation = (ActivityNavigation) context;
        } else {
            throw new IllegalArgumentException(context.getClass().getName() + " not implemented ActivityNavigation");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle b) {
        View root = inflater.inflate(R.layout.fragment_list, container, false);
        recyclerView = root.findViewById(R.id.txt_currency_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        currencyAdapter = new CurrencyAdapter(root.getContext(), ListFragment.this);
        if (rateList == null) {
            processingGetResponseByDate(date, root);
        } else {
            currencyAdapter.setRateList(rateList);
            recyclerView.setAdapter(currencyAdapter);
        }
        return root;
    }

    private void processingGetResponseByDate(String date, View root) {
        ApiService.getData(date)
                .enqueue(new Callback<RequestModel>() {
                    @Override
                    public void onResponse(Call<RequestModel> call, Response<RequestModel> response) {
                        if (response.isSuccessful() && response.body() != null) {
                            rateList = response.body().getExchangeRate();
                            currencyAdapter.setRateList(rateList);
                            recyclerView.setAdapter(currencyAdapter);
                        } else {
                            Toast.makeText(root.getContext(), R.string.error, Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<RequestModel> call, Throwable t) {
                        Toast.makeText(root.getContext(), R.string.error, Toast.LENGTH_SHORT).show();
                    }
                });

    }

    @Override
    public void onCurrencyClick(RequestModel.ExchangeRate currency) {
        navigation.showSingleCurrencyFragment(currency);
    }
}
