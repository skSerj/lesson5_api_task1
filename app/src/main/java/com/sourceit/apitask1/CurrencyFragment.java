package com.sourceit.apitask1;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class CurrencyFragment extends Fragment {

    private String currencyName;
    private Double saleRateNB;
    private Double purchaseRateNB;
    private Double saleRate;
    private Double purchaseRate;

    private TextView viewCurrencyName;
    private TextView viewSaleRateNB;
    private TextView viewPurchaseRateNB;
    private TextView viewSaleRate;
    private TextView viewPurchaseRate;

    public CurrencyFragment(RequestModel.ExchangeRate currency) {
        currencyName = currency.getCurrency();
        saleRateNB = currency.getSaleRateNB();
        purchaseRateNB = currency.getPurchaseRateNB();
        saleRate = currency.getSaleRate();
        purchaseRate = currency.getPurchaseRate();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle b) {
        View v = inflater.inflate(R.layout.fragment_currency, container, false);
        viewCurrencyName = v.findViewById(R.id.fragment_currency_name);
        viewSaleRateNB = v.findViewById(R.id.fragment_currency_sale_rateNB);
        viewPurchaseRateNB = v.findViewById(R.id.fragment_currency_purchase_RateNB);
        viewSaleRate = v.findViewById(R.id.fragment_currency_sale_Rate);
        viewPurchaseRate = v.findViewById(R.id.fragment_currency_purchase_Rate);

        viewCurrencyName.setText(String.format("name: %s", currencyName));
        viewSaleRateNB.setText(String.format("SaleRateNB: %s", saleRateNB));
        viewPurchaseRateNB.setText(String.format("SaleRateNB: %s", purchaseRateNB));
        viewSaleRate.setText(String.format("SaleRateNB: %s", saleRate));
        viewPurchaseRate.setText(String.format("SaleRateNB: %s", purchaseRate));

        return v;
    }
}
