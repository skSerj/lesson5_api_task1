package com.sourceit.apitask1;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.sourceit.apitask1.databinding.FragmentInputDateBinding;


public class InputDateFragment extends Fragment {

    TextView dateFieldError;
    String dateOfRates;
    ActivityNavigation navigation;
    FragmentInputDateBinding binding;

    public static InputDateFragment newInstance() {
        return new InputDateFragment();
    }


    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof ActivityNavigation) {
            navigation = (ActivityNavigation) context;
        } else {
            throw new IllegalArgumentException(context.getClass().getName() + " not implemented ActivityNavigation");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle b) {
        binding = FragmentInputDateBinding.inflate(getLayoutInflater(), container, false);
        View v = binding.getRoot();
        dateFieldError = v.findViewById(R.id.date_field_error);

        binding.btnChangeDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!checkErrors()) {
                    navigation.showCurrencyFragment(binding.inputChangeDate.getText().toString());
                }
            }
        });
        return v;
    }

    private boolean checkErrors() {
        boolean hasError = false;
        if (binding.inputChangeDate.getText().toString().isEmpty()) {
            hasError = true;
            dateFieldError.setVisibility(View.VISIBLE);
        } else {
            dateFieldError.setVisibility(View.GONE);
        }
        return hasError;
    }
}
