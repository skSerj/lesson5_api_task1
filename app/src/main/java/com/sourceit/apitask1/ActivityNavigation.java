package com.sourceit.apitask1;

public interface ActivityNavigation {
    void showSingleCurrencyFragment(RequestModel.ExchangeRate currency);
    void showCurrencyFragment(String inputDate);
}
