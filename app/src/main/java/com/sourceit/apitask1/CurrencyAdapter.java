package com.sourceit.apitask1;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import retrofit2.Callback;

public class CurrencyAdapter extends RecyclerView.Adapter<CurrencyAdapter.CurrencyViewHolder> {

    private Context context;
    private List<RequestModel.ExchangeRate> rateList;
    private OnCurrencyClickListener listener;

    public CurrencyAdapter(Context context, OnCurrencyClickListener listener) {
        this.context = context;
        this.listener = listener;
    }

    public void setRateList(List<RequestModel.ExchangeRate> rateList) {
        this.rateList = rateList;
    }

    @NonNull
    @Override
    public CurrencyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.item_currency, parent, false);
        return new CurrencyViewHolder(v,listener);
    }

    @Override
    public void onBindViewHolder(@NonNull CurrencyViewHolder holder, int position) {
holder.bind(rateList.get(position));
    }

    @Override
    public int getItemCount() {
        return rateList.size();
    }

    static class CurrencyViewHolder extends RecyclerView.ViewHolder {
        OnCurrencyClickListener listener;
        TextView currencyName;
        TextView currencyRateNB;
        View root;

        public CurrencyViewHolder(@NonNull View itemView, OnCurrencyClickListener listener) {
            super(itemView);
            this.listener = listener;

            root = itemView.findViewById(R.id.root);
            currencyName = itemView.findViewById(R.id.currency_name);
            currencyRateNB = itemView.findViewById(R.id.currency_rateNB);
        }

        public void bind(final RequestModel.ExchangeRate currency) {
            currencyName.setText(String.format("Currency: %s",currency.getCurrency()));
            currencyRateNB.setText(String.format("rate: %s",currency.getPurchaseRateNB()));
            root.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onCurrencyClick(currency);
                }
            });
        }
    }
}
