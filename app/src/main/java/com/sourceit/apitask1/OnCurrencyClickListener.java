package com.sourceit.apitask1;

public interface OnCurrencyClickListener {
    void onCurrencyClick(RequestModel.ExchangeRate currency);
}

